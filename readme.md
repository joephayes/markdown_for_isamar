# QGIS Statistics

[QGIS](https://www.qgis.org/en/site/) is able to perform stats depending on the attribute you select.
For example, it can give you information on how many max, min and average for each column. 

![Analysis of dedication field](dedication_analysis.png)

![Analysis of children field](children_analysis.png)

![Analysis of Reign of earliest attestation field](reign_of_earliest_attestation_analysis.png)
 
![Analysis of founders field](founders_analysis.png)
 
![Analysis of seismic class id field](seismic_class_id_analysis.png)